import pandas as pd
import time
import numpy as np
from skranger.ensemble import RangerForestClassifier

train_100k = pd.read_csv('train-1m.csv')
test_all = pd.read_csv('test.csv')

# I should try to use the categorical columns as categories, if that is available, since it is a better way to represent them
# see: https://github.com/imbs-hl/ranger/issues/248#issuecomment-567957783
#
# respect.unordered.factors = "partition"

# although, perhaps it is initially best to just go with whatever raw numeric encoding is provided

# transform Object to Category
all_cat = train_100k.select_dtypes(include=['object']).columns

# plain converting to category does not work, because the values look like 'c-1' and cannot be converted to numbers.
# what about pandas factorize?
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.factorize.html
for cat in all_cat:
    train_100k[cat], train_codes = train_100k[cat].factorize()
    test_all[cat], test_codes = test_all[cat].factorize()

# ML benchmark: ensure they are treated as unordered Factors
train_100k[all_cat] = train_100k[all_cat].astype(pd.CategoricalDtype(ordered=False))
test_all[all_cat] = test_all[all_cat].astype(pd.CategoricalDtype(ordered=False))

# set up training data
X_train = train_100k.iloc[:, :-1]
y_train = train_100k.iloc[:, -1:].values.ravel()

# set up testing data
X_test = test_all.iloc[:, :-1]
y_test = test_all.iloc[:, -1:].values.ravel()

# set up model
# skranger options: https://github.com/crflynn/skranger/blob/master/skranger/ensemble/classifier.py

rf_py_ran = RangerForestClassifier(
    n_estimators = 100, # ML benchmark
    verbose = True, # False in Python, TRUE in R
    mtry = 2, # specifying because the option docs are not 100% clear
    min_node_size = 1,
    max_depth = 20, # ML benchmark
    respect_categorical_features = 'order',
    regularization_factor = None, # default None, basically a vector of 1's
    oob_error = False, # default False in Python, TRUE in R
    n_jobs = 4,
    seed = 42
)

t0 = time.time()
print(time.strftime("%H:%M:%S", time.gmtime(t0)))
thisfit = rf_py_ran.fit(X_train, y_train)
print("done in %0.3fs" % (time.time() - t0))

print(thisfit.ranger_forest_["prediction_error"])