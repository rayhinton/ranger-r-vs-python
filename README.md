# ranger-r-vs-python

Compare the performance of [Ranger](https://github.com/imbs-hl/ranger) as implemented in R and Python.

- R package, `ranger`: https://cran.r-project.org/web/packages/ranger/index.html
- Python package, `skranger`: https://pypi.org/project/skranger/
- Benchmark guidelines: https://github.com/szilard/benchm-ml/tree/master/z-other-tools
